package by.gsu.nkorolev;


import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class VectorTest {
    private static final double X = 1;
    private static final double Y = 2;
    private static final double Z = 3;
    private static final Vector VECTOR = new Vector(X, Y, Z);
    private static final Vector REVERSED = new Vector(Z, Y, X);

    @Test
    public void createsWithSpecifiedCoords() throws Exception {
        assertThat("Created with unspecified x", VECTOR.getX(), is(X));
        assertThat("Created with unspecified y", VECTOR.getY(), is(Y));
        assertThat("Created with unspecified z", VECTOR.getZ(), is(Z));
    }

    @Test
    public void length() throws Exception {
        assertThat("Incorrect length", VECTOR.length(), is(Math.sqrt(X * X + Y * Y + Z * Z)));
    }

    @Test
    public void add() throws Exception {
        assertThat("Incorrect sum of vector", VECTOR.add(REVERSED), is(new Vector(X + Z, Y + Y, Z + X)));
    }

    @Test
    public void sub() throws Exception {
        assertThat("Incorrect substraction of vectors", VECTOR.sub(REVERSED), is(new Vector(X - Z, Y - Y, Z - X)));
    }

    @Test
    public void scalarMultiply() throws Exception {
        int mul = 13;
        assertThat("Incorrect scalar multiply", VECTOR.multiply(mul), is(new Vector(X * mul, Y * mul, Z * mul)));
    }

    @Test
    public void multiplyScalar() throws Exception {
        assertThat("Incorrect multiply scalar", VECTOR.scalarMultiply(REVERSED), is(X * Z + Y * Y + Z * X));
    }

    @Test
    public void vectorMultiply() throws Exception {
        assertThat("Incorrect vector multiply", VECTOR.multiply(REVERSED),
                is(new Vector(Y * X - Z * Y, Z * Z - X * X, X * Y - Z * Y)));

    }
}
