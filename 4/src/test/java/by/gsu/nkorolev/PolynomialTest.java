package by.gsu.nkorolev;


import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class PolynomialTest {
    private static final Polynomial POLY = new Polynomial("x^2+x+24");

    @Test
    public void createsWithSpecifiedPolynomial() throws Exception {
        assertThat("Incorrect parsing", POLY.toString(), is("1x^2+1x^1+24x^0"));
    }

    @Test
    public void add() throws Exception {
        assertThat("Incorrect result of two polynomial sum", POLY.add(POLY).toString(), is("2x^2+2x^1+48x^0"));
    }

    @Test
    public void mulByInteger() throws Exception {
        assertThat("Incorrect multiplying by coef result", POLY.mul(3).toString(), is("3x^2+3x^1+72x^0"));
    }

    @Test
    public void mulByPoly() throws Exception {
        assertThat("Incorrect multiplying by poly result", POLY.mul(POLY).toString(), is("1x^4+2x^3+49x^2+48x^1+576x^0"));
    }

    @Test
    public void sub() throws Exception {
        assertThat("Incorrect substraction result", POLY.sub(POLY).toString(), is("0x^2+0x^1+0x^0"));
    }
}
