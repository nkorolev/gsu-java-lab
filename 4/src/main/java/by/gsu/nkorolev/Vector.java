package by.gsu.nkorolev;

public class Vector {
    final private double x;
    final private double y;
    final private double z;

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public double length() {
        return Math.sqrt(scalarMultiply(this));
    }

    public Vector add(Vector arg) {
        return new Vector(x + arg.x, y + arg.y, z + arg.z);
    }

    public Vector sub(Vector arg) {
        return new Vector(x - arg.x, y - arg.y, z - arg.z);
    }

    public double scalarMultiply(Vector arg) {
        return x * arg.x + y * arg.y + z * arg.z;
    }

    public Vector multiply(double scalar) {
        return new Vector(x * scalar, y * scalar, z * scalar);
    }

    public Vector multiply(Vector arg) {
        return new Vector(y * arg.z - z * arg.y, z * arg.x - arg.z * x, x * arg.y - arg.x * y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector vector = (Vector) o;

        if (Double.compare(vector.x, x) != 0) return false;
        if (Double.compare(vector.y, y) != 0) return false;
        return Double.compare(vector.z, z) == 0;
    }

    @Override
    public String toString() {
        return "Vector{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
