package by.gsu.nkorolev;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polynomial {
    private static final Pattern PATTERN = Pattern.compile("(-?\\d*)[xX](\\^(-?\\d+))?|(-?\\d+)");
    private Map<Integer, Integer> map = new TreeMap<>(Collections.reverseOrder());

    public Polynomial(String string) {
        final Matcher matcher = PATTERN.matcher(string);

        while (matcher.find()) {
            int coef, pow;
            String c = matcher.group(4);
            if (c == null) {
                c = matcher.group(1);
                if (c != null && !c.isEmpty()) {
                    coef = Integer.parseInt(c);
                } else {
                    coef = 1;
                }
                final String p = matcher.group(3);
                if (p != null && !p.isEmpty()) {
                    pow = Integer.parseInt(p);
                } else {
                    pow = 1;
                }
            } else {
                coef = Integer.parseInt(c);
                pow = 0;
            }
            map.put(pow, coef);
        }
    }

    public Polynomial() {
    }

    public Polynomial add(Polynomial poly) {
        Polynomial res = new Polynomial();
        for (Map.Entry<Integer, Integer> powCoefPair : poly.map.entrySet()) {
            res.map.put(powCoefPair.getKey(), powCoefPair.getValue() + map.get(powCoefPair.getKey()));
        }
        return res;
    }

    public Polynomial sub(Polynomial poly) {
        Polynomial res = new Polynomial();
        for (Map.Entry<Integer, Integer> powCoefPair : poly.map.entrySet()) {
            res.map.put(powCoefPair.getKey(), powCoefPair.getValue() - map.get(powCoefPair.getKey()));
        }
        return res;
    }

    public Polynomial mul(int val) {
        Polynomial res = new Polynomial();
        for (Map.Entry<Integer, Integer> powCoefPair : map.entrySet()) {
            res.map.put(powCoefPair.getKey(), powCoefPair.getValue() * val);
        }
        return res;
    }

    public Polynomial mul(Polynomial val) {
        Polynomial res = new Polynomial();
        for (Integer pow1 : map.keySet()) {
            for (Integer pow2 : val.map.keySet()) {
                int pow = pow1 + pow2;
                int coef = map.get(pow1) * val.map.get(pow2);
                Integer oldCoef = res.map.get(pow);
                if (oldCoef != null) {
                    coef += oldCoef;
                }
                res.map.put(pow, coef);
            }
        }
        return res;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            string.append(entry.getValue() + "x^" + entry.getKey() + "+");
        }
        string.setLength(string.length() - 1);

        return string.toString();
    }
}

