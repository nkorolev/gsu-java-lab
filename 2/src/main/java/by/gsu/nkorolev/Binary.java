package by.gsu.nkorolev;


public class Binary {
    public static String toBinaryString(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Argument must be positive int");
        }

        String result = "";
        while (i > 0) {
            result = (i % 2) + result;
            i /= 2;
        }
        return result;
    }
}
