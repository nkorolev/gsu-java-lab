package by.gsu.nkorolev;


public class Newton {
    public static double getRoot(double number, double degree, double acc, double prev) {
        double x = (1 / degree) * ((degree - 1) * prev + number / Math.pow(prev, degree - 1));
        if (Math.abs(x - prev) < acc) {
            return x;
        }
        return getRoot(number, degree, acc, x);
    }
}
