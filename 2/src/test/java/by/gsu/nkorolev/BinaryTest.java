package by.gsu.nkorolev;


import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class BinaryTest {
    @Test(expected = IllegalArgumentException.class)
    public void failsOnNegativeParameter() throws Exception {
        Binary.toBinaryString(-1);
    }

    @Test
    public void convertsToBinaryStringCorrect() throws Exception {
        int number = 325;
        assertThat("Incorrect conversion", Binary.toBinaryString(number), is(Integer.toBinaryString(number)));
    }
}
