package by.gsu.nkorolev;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NewtonTest {
    @Test
    public void calculatesRootOfSpecifiedDegree() throws Exception {
        double x0 = 1, number = 768, root = 8, accuracy = 0.001;
        double result = Newton.getRoot(number, root, accuracy, x0);
        assertThat("Calculated with unspecified precision", result - Math.pow(number, 1 / root) < accuracy,
                is(true));
    }


}
