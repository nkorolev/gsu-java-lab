package by.gsu.nkorolev;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;


public class TriangleTest {
    private static final Point A = spy(new Point(1, 1));
    private static final Point B = spy(new Point(5, 5));
    private static final Point C = spy(new Point(-5, 0));
    private static final Triangle TRIANGLE = new Triangle(A, B, C);

    @Test
    public void createsWithSpecifiedPoints() throws Exception {
        assertThat("Creates with unspecified A point", TRIANGLE.getA(), is(A));
        assertThat("Creates with unspecified B point", TRIANGLE.getB(), is(B));
        assertThat("Creates with unspecified C point", TRIANGLE.getC(), is(C));
    }

    @Test
    public void checksCorrectness() throws Exception {
        assertThat("Error on checking", TRIANGLE.isCorrect(), is(true));
        Triangle t = new Triangle(A, A, C);
        assertThat("Error on checking", t.isCorrect(), is(false));
    }

    @Test
    public void calculatesPerimeter() throws Exception {
        assertThat("Incorrect perimeter", TRIANGLE.getPerimeter(),
                is(A.distance(B) + B.distance(C) + A.distance(C)));
    }

    @Test
    public void calculatesSquare() throws Exception {
        double ab = A.distance(B),
                bc = B.distance(C),
                ac = A.distance(C),
                p = (ab + bc + ac) / 2;

        assertThat("Incorrect square", TRIANGLE.getSquare(),
                is(Math.sqrt(p * (p - ab) * (p - bc) * (p - ac))));
    }

    @Test
    public void movesCorrect() throws Exception {
        double x = 1;
        double y = 2;
        TRIANGLE.move(1, 2);
        verify(A).move(x, y);
        verify(B).move(x, y);
        verify(C).move(x, y);
    }

    @Test
    public void scalesCorrect() throws Exception {
        double x = 1;
        double y = 2;
        TRIANGLE.scale(x, y);
        verify(A).scale(x, y);
        verify(B).scale(x, y);
        verify(C).scale(x, y);
    }

    @Test
    public void rotatesCorrect() throws Exception {
        double angle = 30;
        TRIANGLE.rotate(angle);
        verify(A).rotate(angle);
        verify(B).rotate(angle);
        verify(C).rotate(angle);

    }
}
