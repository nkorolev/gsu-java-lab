package by.gsu.nkorolev;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;


public class PointTest {
    private static final double X = 1;
    private static final double Y = 2;
    private static final Point POINT = new Point(X, Y);

    @Test
    public void createsWithSpecifiedCoordinates() throws Exception {
        assertThat("Creates with unspecified x", POINT.getX(), is(X));
        assertThat("Creates with unspecified y", POINT.getY(), is(Y));
    }

    @Test
    public void calculatesDistance() throws Exception {
        assertThat("Incorrect distance", POINT.distance(POINT), is(0d));
    }

    @Test
    public void movesCorrect() throws Exception {
        Point moved = POINT.move(X, Y);
        assertThat("Incorrect moving on x coord", moved.getX(), is(X + X));
        assertThat("Incorrect moving on y coord", moved.getY(), is(Y + Y));
    }

    @Test
    public void scalesCorrect() throws Exception {
        Point scaled = POINT.scale(X, Y);
        assertThat("Incorrect scaling on x coord", scaled.getX(), is(X * X));
        assertThat("Incorrect scaling on y coord", scaled.getY(), is(Y * Y));
    }

    @Test
    public void rotatesCorrect() throws Exception {
        double angle = 100,
                sin = Math.sin(Math.toRadians(angle)),
                cos = Math.cos(Math.toRadians(angle));
        Point rotated = POINT.rotate(100);
        assertThat("Incorrect rotating on x coord", rotated.getX(), is(X * cos - Y * sin));
        assertThat("Incorrect rotating on y coord", rotated.getY(), is(X * sin + Y * cos));
    }
}
