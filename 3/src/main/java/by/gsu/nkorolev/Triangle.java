package by.gsu.nkorolev;

public class Triangle {
    private Point a;
    private Point b;
    private Point c;
    private double ab;
    private double bc;
    private double ac;

    public Triangle(Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;

        ab = a.distance(b);
        bc = b.distance(c);
        ac = a.distance(c);
    }

    public boolean isCorrect() {
        return ab + bc > ac && ab + ac > bc && bc + ac > ab;
    }

    public Point getA() {
        return a;
    }

    public Point getB() {
        return b;
    }

    public Point getC() {
        return c;
    }

    public double getPerimeter() {
        return ab + bc + ac;
    }

    public double getSquare() {
        double p = getPerimeter() / 2;
        return Math.sqrt(p * (p - ab) * (p - bc) * (p - ac));
    }

    public Triangle move(double x, double y) {
        return new Triangle(a.move(x, y), b.move(x, y), c.move(x, y));
    }

    public Triangle scale(double x, double y) {
        return new Triangle(a.scale(x, y), b.scale(x, y), c.scale(x, y));
    }

    public Triangle rotate(double angle) {
        return new Triangle(a.rotate(angle), b.rotate(angle), c.rotate(angle));
    }


}
