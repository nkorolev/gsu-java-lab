package by.gsu.nkorolev;

public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distance(Point b) {
        return Math.sqrt(Math.pow(b.x - x, 2) + Math.pow(b.y - y, 2));
    }

    public Point move(double x, double y) {
        return new Point(this.x + x, this.y + y);
    }

    public Point scale(double x, double y) {
        return new Point(this.x * x, this.y * y);
    }

    public Point rotate(double angle) {
        double cos = Math.cos(Math.toRadians(angle)),
                sin = Math.sin(Math.toRadians(angle));
        return new Point(x * cos - y * sin, x * sin + y * cos);
    }
}
