<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Image</title>
</head>
<body>
    <form method="post" action="/img">
        <input type="text" placeholder="Type a theme" name="q"/>
        <input type="submit" value="Show me">
    </form>
    <c:if test="${not empty img}">
        <h1>Result:</h1>
        <p>Please wait. Image loading may take a while.</p>
        <img src="${img}" alt="">
    </c:if>
</body>
</html>
