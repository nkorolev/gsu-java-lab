<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<c:choose>
    <c:when test="${not empty res}">
        <h1>${function}(${arg})=${res}</h1>
    </c:when>
    <c:otherwise>
        <form action="/math" method="post">
            <label for="f">Select function</label>
            <select name="function" id="f">
                <c:forEach var="f" items="${functions}">
                    <option value="${f}">${f}</option>
                </c:forEach>
            </select>
            <label for="a">Type double arg:</label>
            <input type="text" id="a" name="arg"/>
            <label for="p">Precision:</label>
            <input type="text" name="pre" id="p">
            <input type="checkbox" name="rad"> Convert to rad
            <input type="submit" value="Ok"/>
        </form>
    </c:otherwise>
</c:choose>
</body>
</html>