<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Scanner" %>
<%@ page import="java.util.regex.Pattern" %>
<%@ page import="java.util.Date" %>
<%@ page import="by.gsu.nkorolev.servlet.AboutServlet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%!
    private static class Entry {
        String name;
        String phone;
        String address;

        public Entry(String csv) {
            String[] pieces = csv.split(";");
            name = pieces[0];
            phone = pieces[1];
            address = pieces[2];
        }

        boolean matchName(String q) {
            return Pattern.compile(q).matcher(name.toLowerCase()).find();
        }

        boolean matchPhone(String q) {
            return Pattern.compile(q).matcher(phone.toLowerCase()).find();
        }
    }
%>

<%!
    private List<Entry> entries = new ArrayList<>();

    public void jspInit() {
        InputStream stream = null;
        Scanner sc = null;
        try {
            stream = getServletContext().getResourceAsStream("/WEB-INF/data.csv");
            sc = new Scanner(stream);

            while (sc.hasNextLine()) {
                entries.add(new Entry(sc.nextLine()));
            }

        } finally {
            try {
                sc.close();
            } catch (Exception e) {
                //do nothing
            }
            try {
                stream.close();
            } catch (Exception e) {
                //do nothing
            }
        }
    }
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table width="100%" border="1">
    <tr>
        <th>Name</th>
        <th>Phone</th>
        <th>Address</th>
    </tr>
    <%
        for (Entry entry : entries) {
            String nameFilter = request.getParameter("name");
            String phoneFilter = request.getParameter("phone");
            if (nameFilter == null || nameFilter.isEmpty()) {
                nameFilter = ".*";
            }
            if (phoneFilter == null || phoneFilter.isEmpty()) {
                phoneFilter = ".*";
            }
            if (!entry.matchName(nameFilter) || !entry.matchPhone(phoneFilter)) {
                continue;
            }
    %>
    <tr>
        <td><%=entry.name%>
        </td>
        <td><%=entry.phone%>
        </td>
        <td><%=entry.address%>
        </td>
    </tr>
    <% } %>
</table>
<hr>
<form action="/book.jsp" method="get">
    <label for="name">Name filter:</label>
    <input type="text" name="name" id="name" value="${param.name}" autofocus/>
    <label for="phone">Phone filter:</label>
    <input type="text" name="phone" id="phone" value="${param.phone}">
    <input type="submit" value="Filter">
    <p>* - make all field empty to show all</p>
</form>
<p><b>Время генерации страницы: <%=AboutServlet.DATE_FORMATTER.format(new Date())%></b></p>
</body>
</html>
