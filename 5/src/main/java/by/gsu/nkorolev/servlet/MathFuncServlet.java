package by.gsu.nkorolev.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.HashSet;
import java.util.Set;

@WebServlet(urlPatterns = "/math")
public class MathFuncServlet extends HttpServlet {
    private static final Set<String> FUNCTIONS = new HashSet<>();

    static {
        for (Method method : Math.class.getDeclaredMethods()) {
            Parameter[] params = method.getParameters();
            if (params.length != 1 || params[0].getType().equals(Double.class)) {
                continue;
            }
            FUNCTIONS.add(method.getName());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("functions", FUNCTIONS);
        req.getRequestDispatcher("/WEB-INF/math.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String arg = req.getParameter("arg");
        String function = req.getParameter("function");
        String precision = req.getParameter("pre");
        boolean toRad = req.getParameter("rad") != null;
        if (arg == null || function == null) {
            throw new IllegalArgumentException("Please specify function and (or) argument.");
        }
        try {
            double doubleArg = Double.parseDouble(arg);
            if (!toRad) {
                doubleArg = Math.toRadians(doubleArg);
            }
            Object res = Math.class.getDeclaredMethod(function, double.class).invoke(null, doubleArg);
            req.setAttribute("arg", arg);
            req.setAttribute("function", function);
            req.setAttribute("res", String.format("%." + Long.parseLong(precision) + "f", res));
            req.getRequestDispatcher("/WEB-INF/math.jsp").forward(req, resp);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Arg should be valid double and (or) precision should be valid long.", e);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException("Incorrect function name.", e);
        }

    }
}
