package by.gsu.nkorolev.servlet;

import by.gsu.nkorolev.util.RawResponse;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/temp")
public class TempServlet extends HttpServlet {
    private static final String TEMP_TEMPLATE = "<span style='color: %s'>%s</span> is %s";
    private static final String FORM_TEMPLATE = "<form action='' method='get'>" +
            "<input type='text' name='temp' placeholder='Enter value'/>" +
            "<input type='submit' value='Check'/></form>";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RawResponse rawResponse = new RawResponse(resp);
        rawResponse.write(FORM_TEMPLATE);
        try {
            String temp = req.getParameter("temp");
            if (temp == null) {
                return;
            }
            if (Double.parseDouble(temp) < 0) {
                rawResponse.write(TEMP_TEMPLATE, "blue", temp, "cold");
            } else {
                rawResponse.write(TEMP_TEMPLATE, "red", temp, "hot");
            }
            rawResponse.flush();
        } catch (NumberFormatException e) {
            rawResponse.write("Plz specify valid temp.");
        }
    }
}
