package by.gsu.nkorolev.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet(urlPatterns = "/img")
public class ImageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/img.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String query = req.getParameter("q");
        if (query == null) {
            throw new IllegalArgumentException("Query must be specified.");
        }
        query = query.trim().replaceAll("\\s+", ",");
        req.setAttribute("img", "http://loremflickr.com/800/600/"
                + URLEncoder.encode(query.trim().replaceAll("\\s+", ","), "utf-8")
                + "/all");
        req.getRequestDispatcher("/WEB-INF/img.jsp").forward(req, resp);
    }
}
