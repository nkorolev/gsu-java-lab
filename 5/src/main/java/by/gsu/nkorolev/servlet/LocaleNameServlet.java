package by.gsu.nkorolev.servlet;


import by.gsu.nkorolev.util.RawResponse;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@WebServlet(urlPatterns = "/locale")
public class LocaleNameServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        new RawResponse(resp)
                .writeLine("Host system country: " + Locale.getDefault().getDisplayCountry())
                .writeLine("Host system lang: " + Locale.getDefault().getDisplayLanguage())
                .flush();
    }
}
