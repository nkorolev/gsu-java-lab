package by.gsu.nkorolev.servlet;

import by.gsu.nkorolev.util.RawResponse;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(urlPatterns = "/about")
public class AboutServlet extends HttpServlet {
    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd.MM.yyyy 'в' HH:mm:ss z");
    private static final Date RECEIVED_DATE = new Date(1461153600000L);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        new RawResponse(resp)
                .writeLine("Author: Nikita Korolev")
                .write("Received: %s%n", DATE_FORMATTER.format(RECEIVED_DATE))
                .writeLine()
                .write("Done: %s%n", DATE_FORMATTER.format(new Date())) //now
                .writeLine()
                .flush();
    }
}
