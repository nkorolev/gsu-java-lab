package by.gsu.nkorolev.util;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RawResponse {
    private static final String BREAK = "<BR>";
    private final HttpServletResponse resp;

    public RawResponse(HttpServletResponse resp) {
        this.resp = resp;
        this.resp.setContentType("text/html");
        this.resp.setCharacterEncoding("UTF8");
    }

    public RawResponse write(String data) throws IOException {
        resp.getWriter().write(data);
        return this;
    }

    public RawResponse writeLine(String data) throws IOException {
        write(data);
        writeLine();
        return this;
    }

    public RawResponse writeLine() throws IOException {
        write(BREAK);
        return this;
    }

    public RawResponse write(String template, Object... args) throws IOException {
        write(String.format(template, args));
        return this;
    }

    public RawResponse flush() throws IOException {
        resp.getWriter().flush();
        return this;
    }
}
