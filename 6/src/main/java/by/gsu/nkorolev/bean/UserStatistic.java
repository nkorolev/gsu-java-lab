package by.gsu.nkorolev.bean;

import java.sql.Date;

public class UserStatistic extends User {
    private int sended;
    private int received;

    public UserStatistic(int id, String name, Date date, int sended, int received) {
        super(id, name, date);
        this.sended = sended;
        this.received = received;
    }

    public UserStatistic(User user, int sended, int received) {
        super(user.id, user.name, user.date);
        this.sended = sended;
        this.received = received;
    }

    public int getSended() {
        return sended;
    }

    public void setSended(int sended) {
        this.sended = sended;
    }

    public int getReceived() {
        return received;
    }

    public void setReceived(int received) {
        this.received = received;
    }
}
