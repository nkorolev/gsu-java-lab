package by.gsu.nkorolev.dao;


import by.gsu.nkorolev.bean.User;
import by.gsu.nkorolev.bean.UserStatistic;
import by.gsu.nkorolev.sql.Query;

import java.util.List;

/**
 * Contains additional tasks logic
 */
public class AdditionalTaskDAO {
    public enum Type {
        RECEIVE {
            @Override
            public String modifier() {
                return "";
            }
        },
        NOT_RECEIVE {
            @Override
            public String modifier() {
                return "NOT";
            }
        };

        public abstract String modifier();
    }

    public User getUserWithShortestMailLength() {
        return new Query().execute("select * from users " +
                "inner join (select tid, sum(length(mtext)) as length from mails group by tid) as t on t.tid = id " +
                "order by length asc limit 1", UsersDAO.USER_MAPPER).get(0);
    }

    public List<UserStatistic> getUsersWithStatistic() {
        return new Query().execute("select *, (select count(*) from mails where fid = users.id) as sended, " +
                        "(select count(*) from mails where tid= users.id) as received from users",
                rs -> new UserStatistic(UsersDAO.USER_MAPPER.map(rs), rs.getInt("sended"), rs.getInt("received")));
    }

    public List<User> getUserWhich(Type type, String subject) {
        return new Query().execute("select * from users " +
                        "where " + type.modifier() + " exists(select * from mails where tid = users.id and subject like ?)",
                UsersDAO.USER_MAPPER, "%" + subject + "%");
    }

    public void sendMailToAllUsers(int id) {
        new Query().execute("insert into mails (fid, tid, subject, mtext, sdate) " +
                "select o.fid, u.id, o.subject, o.mtext, o.sdate from mails as o, users as u where o.id = ?", id);
    }
}
