package by.gsu.nkorolev.dao;


import by.gsu.nkorolev.bean.User;
import by.gsu.nkorolev.sql.CRUDDao;
import by.gsu.nkorolev.sql.Mapper;
import by.gsu.nkorolev.sql.Query;

import java.util.List;

public class UsersDAO implements CRUDDao<User> {
    public static Mapper<User> USER_MAPPER = rs -> new User(rs.getInt("id"), rs.getString("full_name"), rs.getDate("bdate"));

    @Override
    public List<User> get() {
        return new Query().execute("SELECT * FROM users", USER_MAPPER);
    }

    @Override
    public int add(User instance) {
        return new Query().execute("INSERT INTO users (full_name, bdate) values(?, ?)",
                instance.getName(), instance.getDate());
    }

    @Override
    public void delete(int id) {
        new Query().execute("DELETE FROM users WHERE id = ?", id);
    }
}
