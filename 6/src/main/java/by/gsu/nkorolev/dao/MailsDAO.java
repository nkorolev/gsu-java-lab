package by.gsu.nkorolev.dao;

import by.gsu.nkorolev.bean.Mail;
import by.gsu.nkorolev.bean.User;
import by.gsu.nkorolev.sql.CRUDDao;
import by.gsu.nkorolev.sql.Query;

import java.util.List;


public class MailsDAO implements CRUDDao<Mail> {
    @Override
    public List<Mail> get() {
        throw new UnsupportedOperationException();
    }

    public List<Mail> get(int uid) {
        return new Query().execute("SELECT mails.*, f.full_name as fname, f.bdate as fbdate, t.full_name as tname, t.bdate as tbdate " +
                "from mails inner join users as f on mails.fid = f.id " +
                "inner join users as t on mails.tid = t.id where tid = ?", rs -> new Mail(rs.getInt("id"),
                new User(rs.getInt("fid"), rs.getString("fname"), rs.getDate("fbdate")),
                new User(rs.getInt("tid"), rs.getString("tname"), rs.getDate("tbdate")),
                rs.getString("subject"), rs.getString("mtext"), rs.getDate("sdate")), uid);
    }

    @Override
    public int add(Mail instance) {
        return new Query().execute("INSERT INTO mails (fid, tid, subject, mtext, sdate) VALUES(?, ?, ?, ?, ?)",
                instance.getFrom().getId(), instance.getTo().getId(), instance.getSubject(),
                instance.getText(), instance.getDate());
    }

    @Override
    public void delete(int id) {
        new Query().execute("DELETE FROM mails WHERE id = ?", id);

    }
}
