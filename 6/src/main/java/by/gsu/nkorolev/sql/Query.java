package by.gsu.nkorolev.sql;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class Query {
    private static final Connection CONNECTION;

    static {
        try {
            URI dbUri = new URI(System.getenv("DATABASE_URL"));

            String username = dbUri.getUserInfo().split(":")[0];
            String password = dbUri.getUserInfo().split(":")[1];
            //TODO: ssl
            String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath() + "?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";

            CONNECTION = DriverManager.getConnection(dbUrl, username, password);
        } catch (URISyntaxException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static PreparedStatement createPreparedStatement(String q, Object... params) throws SQLException {
        //TODO: return keys
        PreparedStatement ps = CONNECTION.prepareStatement(q);
        for (int i = 0; i < params.length; i++) {
            ps.setObject(i + 1, params[i]);
        }
        return ps;
    }

    public <T> List<T> execute(String query, Mapper<T> mapper, Object... params) {
        try (PreparedStatement ps = createPreparedStatement(query, params);
             ResultSet rs = ps.executeQuery()) {

            List<T> result = new ArrayList<>();
            while (rs.next()) {
                result.add(mapper.map(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int execute(String query, Object... params) {
        try (PreparedStatement ps = createPreparedStatement(query, params)) {
            return ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
