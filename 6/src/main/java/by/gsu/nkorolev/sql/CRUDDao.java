package by.gsu.nkorolev.sql;

import java.util.List;

public interface CRUDDao<T> {
    List<T> get();

    int add(T instance);

    void delete(int id);
}
