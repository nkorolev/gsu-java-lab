<%@ page import="by.gsu.nkorolev.dao.MailsDAO" %>
<%@ page import="by.gsu.nkorolev.bean.Mail" %>
<%@ page import="java.util.List" %>
<%
    int uid = Integer.parseInt(request.getParameter("id"));
    List<Mail> mails = new MailsDAO().get(uid);
%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<h1>Mailbox</h1>
<%
    for (Mail mail : mails) {
%>
    <div style="border: 1px solid black; padding: 10px;">
        <p>FROM: <a href="mails.jsp?id=<%=mail.getFrom().getId()%>"><%=mail.getFrom().getName()%></a></p>
        <a href="delete_mail.jsp?id=<%=mail.getId()%>">Delete</a>
        <a href="send_to_all.jsp?id=<%=mail.getId()%>">Send to all</a>
        <p><%=mail.getSubject()%></p>
        <hr>
        <div>
            <%=mail.getText()%>
        </div>
    </div>
<%
    }
%>
<a href="/send_mail.jsp">Send message</a>
</body>
</html>