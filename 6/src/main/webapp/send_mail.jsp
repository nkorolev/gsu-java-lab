<%@ page import="by.gsu.nkorolev.dao.UsersDAO" %>
<%@ page import="by.gsu.nkorolev.bean.User" %>
<%@ page import="java.util.List" %>
<%@ page import="by.gsu.nkorolev.dao.MailsDAO" %>
<%@ page import="by.gsu.nkorolev.bean.Mail" %>
<%@ page import="java.util.Date" %>
<%
    if (request.getMethod().toLowerCase().equals("post")) {
        int fid = Integer.parseInt(request.getParameter("from"));
        int tid = Integer.parseInt(request.getParameter("to"));
        String subj = request.getParameter("subj");
        String text = request.getParameter("text");
        new MailsDAO().add(new Mail(0, new User(fid, null, null), new User(tid, null, null),
                subj, text, new java.sql.Date(new Date().getTime())));
        response.sendRedirect("/index.jsp");
    }

    List<User> users = new UsersDAO().get();
%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<h1>Send mail</h1>
<form action="/send_mail.jsp" method="post">
    <select name="from">
        <option disabled selected>From</option>
        <% for (User user : users) {%>
            <option value="<%=user.getId()%>"><%=user.getName()%></option>
        <%}%>
    </select>
    <br>
    <select name="to">
        <option disabled selected>To</option>
        <% for (User user : users) {%>
            <option value="<%=user.getId()%>"><%=user.getName()%></option>
        <%}%>
    </select>
    <br>
    <input type="text" placeholder="Enter subject" name="subj">
    <br>
    <textarea name="text" id="" cols="30" rows="10" placeholder="Enter text"></textarea>
    <br>
    <input type="submit" value="Send">
</form>
</body>
</html>