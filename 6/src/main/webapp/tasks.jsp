<%@ page import="by.gsu.nkorolev.dao.AdditionalTaskDAO" %>
<%@ page import="by.gsu.nkorolev.bean.UserStatistic" %>
<%@ page import="by.gsu.nkorolev.bean.User" %>
<%!
    public AdditionalTaskDAO taskDAO = new AdditionalTaskDAO();
%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<b>User with shortest mail length:</b> <%=taskDAO.getUserWithShortestMailLength()%>
<hr>
<b>User stats:</b>
<table>
    <tr>
        <td>Name</td>
        <td>Sended</td>
        <td>Received</td>
    </tr>

<% for (UserStatistic userStatistic : taskDAO.getUsersWithStatistic()) { %>
        <tr>
            <td><%=userStatistic.getName()%></td>
            <td><%=userStatistic.getSended()%></td>
            <td><%=userStatistic.getReceived()%></td>
        </tr>
<% } %>
</table>
<hr>
<form action="/tasks.jsp" method="post">
    <input type="text" placeholder="Enter subject" name="subj">
    <input type="submit" value="Send">
</form>
<%
    if (request.getMethod().toLowerCase().equals("post")) {
        String subj = request.getParameter("subj");
        System.err.println(subj);
%>
<b>Receive email with '<%=subj%>'</b>
<ul>
    <% for (User user : taskDAO.getUserWhich(AdditionalTaskDAO.Type.RECEIVE, subj)) {%>
        <li><%=user.getName()%></li>
    <%}%>
</ul>
<b>Not receive email with '<%=subj%>'</b>
<ul>
    <% for (User user : taskDAO.getUserWhich(AdditionalTaskDAO.Type.NOT_RECEIVE, subj)) {%>
        <li><%=user.getName()%></li>
    <%}%>
</ul>
<%}%>

<a href="index.jsp">Go to main</a>

</body>
</html>