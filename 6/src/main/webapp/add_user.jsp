<%@ page import="by.gsu.nkorolev.bean.User" %>
<%@ page import="by.gsu.nkorolev.dao.UsersDAO" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%
    try {
        String name = request.getParameter("name");
        String date = request.getParameter("bdate");
        if (name != null && date != null) {
            Date bdate = new SimpleDateFormat("dd.MM.yyyy").parse(date);
            if (name == null) {
                throw new IllegalArgumentException("Name must be specified");
            }
            new UsersDAO().add(new User(0, name, new java.sql.Date(bdate.getTime())));
            response.sendRedirect("/index.jsp");
        }
    } catch (ParseException e) {
        throw new IllegalArgumentException("Birthdate must be correct");
    }

%>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<h1>Add user</h1>
<form action="/add_user.jsp" method="post">
    <label><input type="text" placeholder="Enter name" name="name"></label>
    <br>
    <label><input type="text" placeholder="Enter birthdate (dd.mm.yyyy)" name="bdate"></label>
    <br>
    <input type="submit" value="Add user">
</form>
</body>
</html>