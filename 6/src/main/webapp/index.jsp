<%@ page import="by.gsu.nkorolev.bean.User" %>
<%@ page import="by.gsu.nkorolev.dao.UsersDAO" %>
<%@ page import="java.util.List" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<h1>User list</h1>
<ul>
    <%
        List<User> users = new UsersDAO().get();
        for (User user : users) {
    %>
            <li><a href="mails.jsp?id=<%=user.getId()%>"><%=user.getName()%></a> [<%=user.getDate()%>] <a href="delete_user.jsp?id=<%=user.getId()%>">x</a></li>
    <%
        }
    %>
</ul>
<a href="/add_user.jsp">Add new</a>
<a href="/send_mail.jsp">Send message</a>
<a href="/tasks.jsp">See additional tasks</a>
</body>
</html>
